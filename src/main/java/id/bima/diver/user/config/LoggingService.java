package id.bima.diver.user.config;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface LoggingService {
    
    public void logRequest(HttpServletRequest httpServletRequest, Object body);
    
    public void logResponse(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object body);

}
