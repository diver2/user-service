package id.bima.diver.user.config;

import org.springframework.context.annotation.Configuration;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import id.bima.diver.common.constant.HeaderKey;
import id.bima.diver.common.util.HttpServletUtils;

@Configuration
public class FeignRequestInterceptor implements RequestInterceptor {
	
	@Override
	public void apply(RequestTemplate template) {
		template.header(HeaderKey.USER_ID, HttpServletUtils.getUserId().toString());
	}

}
