package id.bima.diver.user.config;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;

import id.bima.diver.common.util.HttpServletUtils;

public class AuditAware implements AuditorAware<Long> {
	
	@Override
	public Optional<Long> getCurrentAuditor() {
		return Optional.of(HttpServletUtils.getUserId());
	}
	
}
