package id.bima.diver.user.config;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import id.bima.diver.common.constant.HeaderKey;
import id.bima.diver.common.constant.MdcKey;
import id.bima.diver.common.util.HttpServletUtils;
import id.bima.diver.common.util.SecureRandomUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class EndpointInterceptor implements HandlerInterceptor {
	
	private static final String HEALTH_CHECK_ENDPOINT = "actuator/health";
	
	@Value("${spring.application.name}")
	private String appName;
	
	private Environment env;
	
	@Autowired
	public EndpointInterceptor(Environment env) {
		super();
		this.env = env;
	}
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		setMdc();
		if (!isHealthCheck(request)) {
			String prop =  request.getServletPath().substring(1).replace("\\/", ".");
			boolean isEnabled = env.getProperty(prop, Boolean.class, Boolean.TRUE);
			log.info("Is endpoint enabled : {}", isEnabled);
			if (!isEnabled) {
				response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "API is disabled");
				return false;
			}
		}
		
		return true;
	}
	
	private boolean isHealthCheck(HttpServletRequest request) {
		return request.getRequestURI().contains(HEALTH_CHECK_ENDPOINT);
	}
	
	private void setMdc() {
		// Put User Id
		Long userId = HttpServletUtils.getUserId();
		MDC.put(MdcKey.MDC_KEY_TRACE_ID, userId.toString());
		
		// Put Trace Id
		String traceId = SecureRandomUtil.getRandomString(10, SecureRandomUtil.ALPHANUMERIC);
		MDC.put(MdcKey.MDC_KEY_TRACE_ID, traceId);
		
		// Put Parent Trace Id
		String parentTraceId = HttpServletUtils.getAttributeValue(HeaderKey.TRACE_ID);
		MDC.put(MdcKey.MDC_KEY_PARENT_TRACE_ID, StringUtils.isEmpty(parentTraceId) ? traceId : parentTraceId);
	}

}
