package id.bima.diver.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.UserGroupDto;
import id.bima.diver.user.entity.UserGroup;
import id.bima.diver.user.repository.UserGroupRepository;
import id.bima.diver.user.service.UserGroupService;
import id.bima.diver.user.util.UserGroupDtoUtil;

@RestController
@RequestMapping(UserPath.USER_GROUP_V1)
public class UserGroupController extends DefaultDatabaseController<UserGroupService, UserGroupRepository, UserGroupDtoUtil, UserGroupDto, UserGroup, Long> {

	@Autowired
	protected UserGroupController(UserGroupService service) {
		super(service);
	}
	
}
