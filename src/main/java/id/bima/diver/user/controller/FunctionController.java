package id.bima.diver.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.FunctionDto;
import id.bima.diver.user.entity.Function;
import id.bima.diver.user.repository.FunctionRepository;
import id.bima.diver.user.service.FunctionService;
import id.bima.diver.user.util.FunctionDtoUtil;

@RestController
@RequestMapping(UserPath.FUNCTION_V1)
public class FunctionController extends DefaultDatabaseController<FunctionService, FunctionRepository, FunctionDtoUtil, FunctionDto, Function, Long> {

	@Autowired
	protected FunctionController(FunctionService service) {
		super(service);
	}

}
