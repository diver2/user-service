package id.bima.diver.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.RoleDto;
import id.bima.diver.user.entity.Role;
import id.bima.diver.user.repository.RoleRepository;
import id.bima.diver.user.service.RoleService;
import id.bima.diver.user.util.RoleDtoUtil;

@RestController
@RequestMapping(UserPath.ROLE_V1)
public class RoleController extends DefaultDatabaseController<RoleService, RoleRepository, RoleDtoUtil, RoleDto, Role, Long> {

	@Autowired
	protected RoleController(RoleService service) {
		super(service);
	}

}
