package id.bima.diver.user.controller;

import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.GetUserAuthRequestDto;
import id.bima.diver.user.dto.GetUserAuthResponseDto;
import id.bima.diver.user.dto.UserAuthDto;
import io.swagger.v3.oas.annotations.Operation;

@Lazy
public interface UserAuthApi extends DefaultDatabaseApi<UserAuthDto, Long> {
	
	@Operation(summary = "API to get user auth list by user id from database")
	@PostMapping(value = UserPath.GET_LIST_BY_USER_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetUserAuthResponseDto> getListByUserId(@RequestBody GetUserAuthRequestDto request);
	
	@Operation(summary = "API to get user auth list by user id and is deleted from database")
	@PostMapping(value = UserPath.GET_LIST_BY_USER_ID_AND_IS_DELETED, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetUserAuthResponseDto> getListByUserIdAndIsDeleted(@RequestBody GetUserAuthRequestDto request);
	
	@Operation(summary = "API to get user auth list by user name from database")
	@PostMapping(value = UserPath.GET_LIST_BY_USER_NAME, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetUserAuthResponseDto> getListByUserName(@RequestBody GetUserAuthRequestDto request);
	
	@Operation(summary = "API to get user auth list by user name and is deleted from database")
	@PostMapping(value = UserPath.GET_LIST_BY_USER_NAME_AND_IS_DELETED, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetUserAuthResponseDto> getListByUserNameAndIsDeleted(@RequestBody GetUserAuthRequestDto request);
	
}
