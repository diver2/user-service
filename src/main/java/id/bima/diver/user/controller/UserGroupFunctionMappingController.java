package id.bima.diver.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.DeleteUserGroupFunctionMappingRequestDto;
import id.bima.diver.user.dto.DeleteUserGroupFunctionMappingResponseDto;
import id.bima.diver.user.dto.GetFunctionIdRequestDto;
import id.bima.diver.user.dto.GetFunctionIdResponseDto;
import id.bima.diver.user.dto.GetUserGroupIdRequestDto;
import id.bima.diver.user.dto.GetUserGroupIdResponseDto;
import id.bima.diver.user.dto.UserGroupFunctionMappingDto;
import id.bima.diver.user.entity.UserGroupFunctionMapping;
import id.bima.diver.user.entity.UserGroupFunctionMappingPk;
import id.bima.diver.user.repository.UserGroupFunctionMappingRepository;
import id.bima.diver.user.service.UserGroupFunctionMappingService;
import id.bima.diver.user.util.UserGroupFunctionMappingDtoUtil;

@RestController
@RequestMapping(UserPath.USER_GROUP_FUNCTION_MAPPING_V1)
public class UserGroupFunctionMappingController 
		extends CompositeDatabaseController<UserGroupFunctionMappingService, UserGroupFunctionMappingRepository, UserGroupFunctionMappingDtoUtil, UserGroupFunctionMappingDto, UserGroupFunctionMapping, UserGroupFunctionMappingPk>
		implements UserGroupFunctionMappingApi {
	
	@Autowired
	protected UserGroupFunctionMappingController(UserGroupFunctionMappingService service) {
		super(service);
	}

	@Override
	public BaseResponse<DeleteUserGroupFunctionMappingResponseDto> deleteUserGroupFunctionMappingListByFunctionId(DeleteUserGroupFunctionMappingRequestDto request) {
		return service.deleteUserGroupFunctionMappingListByFunctionId(request);
	}

	@Override
	public BaseResponse<DeleteUserGroupFunctionMappingResponseDto> deleteUserGroupFunctionMappingListByUserGroupId(DeleteUserGroupFunctionMappingRequestDto request) {
		return service.deleteUserGroupFunctionMappingListByUserGroupId(request);
	}

	@Override
	public BaseResponse<GetFunctionIdResponseDto> getFunctionIdListByUserGroupId(@RequestBody GetFunctionIdRequestDto request) {
		return service.getFunctionIdListByUserGroupId(request);
	}
	
	@Override
	public BaseResponse<GetUserGroupIdResponseDto> getUserGroupIdListByFunctionId(@RequestBody GetUserGroupIdRequestDto request) {
		return service.getUserGroupIdListByFunctionId(request);
	}
	
}
