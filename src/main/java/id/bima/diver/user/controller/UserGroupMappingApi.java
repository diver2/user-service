package id.bima.diver.user.controller;

import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.DeleteUserGroupMappingRequestDto;
import id.bima.diver.user.dto.DeleteUserGroupMappingResponseDto;
import id.bima.diver.user.dto.GetUserGroupIdRequestDto;
import id.bima.diver.user.dto.GetUserGroupIdResponseDto;
import id.bima.diver.user.dto.GetUserIdRequestDto;
import id.bima.diver.user.dto.GetUserIdResponseDto;
import id.bima.diver.user.dto.UserGroupMappingDto;
import io.swagger.v3.oas.annotations.Operation;

@Lazy
public interface UserGroupMappingApi extends CompositeDatabaseApi<UserGroupMappingDto>  {
	
	@Operation(summary = "API to delete user group mapping list by user group id from database")
	@PostMapping(value = UserPath.DELETE_BY_USER_GROUP_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<DeleteUserGroupMappingResponseDto> deleteUserGroupMappingListByUserGroupId(@RequestBody DeleteUserGroupMappingRequestDto request);

	@Operation(summary = "API to delete user group mapping list by user id from database")
	@PostMapping(value = UserPath.DELETE_BY_USER_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<DeleteUserGroupMappingResponseDto> deleteUserGroupMappingListByUserId(@RequestBody DeleteUserGroupMappingRequestDto request);
	
	@Operation(summary = "API to get user group id list by user id from database")
	@PostMapping(value = UserPath.GET_USER_GROUP_ID_LIST_BY_USER_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetUserGroupIdResponseDto> getUserGroupIdListByUserId(@RequestBody GetUserGroupIdRequestDto request);
	
	@Operation(summary = "API to get user id list by user group id from database")
	@PostMapping(value = UserPath.GET_USER_ID_LIST_BY_USER_GROUP_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetUserIdResponseDto> getUserIdListByUserGroupId(@RequestBody GetUserIdRequestDto request);
	
}
