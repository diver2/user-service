package id.bima.diver.user.controller;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.GetUserRequestDto;
import id.bima.diver.user.dto.GetUserResponseDto;
import id.bima.diver.user.dto.UserDto;
import id.bima.diver.user.dto.UserGroupResponse;
import io.swagger.v3.oas.annotations.Operation;

@Lazy
public interface UserApi extends DefaultDatabaseApi<UserDto, Long> {
	
	@Operation(summary = "API to get user al data by user id from database")
	@PostMapping(value = UserPath.GET_USER_FULL, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetUserResponseDto> getUserFullById(@RequestBody GetUserRequestDto request);


	@Operation(summary = "API to get user al data by user id from database")
	@PostMapping(value = UserPath.GET_ALL_USER_ROLE)
	public BaseResponse<List<UserGroupResponse>> getAllUserRole();

}
