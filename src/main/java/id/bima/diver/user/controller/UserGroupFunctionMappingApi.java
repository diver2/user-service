package id.bima.diver.user.controller;

import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.DeleteUserGroupFunctionMappingRequestDto;
import id.bima.diver.user.dto.DeleteUserGroupFunctionMappingResponseDto;
import id.bima.diver.user.dto.GetFunctionIdRequestDto;
import id.bima.diver.user.dto.GetFunctionIdResponseDto;
import id.bima.diver.user.dto.GetUserGroupIdRequestDto;
import id.bima.diver.user.dto.GetUserGroupIdResponseDto;
import id.bima.diver.user.dto.UserGroupFunctionMappingDto;
import io.swagger.v3.oas.annotations.Operation;

@Lazy
public interface UserGroupFunctionMappingApi extends CompositeDatabaseApi<UserGroupFunctionMappingDto> {
	
	@Operation(summary = "API to delete user group function mapping list by function id from database")
	@PostMapping(value = UserPath.DELETE_BY_FUNCTION_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<DeleteUserGroupFunctionMappingResponseDto> deleteUserGroupFunctionMappingListByFunctionId(@RequestBody DeleteUserGroupFunctionMappingRequestDto request);
	
	@Operation(summary = "API to delete user group function mapping list by user group id from database")
	@PostMapping(value = UserPath.DELETE_BY_USER_GROUP_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<DeleteUserGroupFunctionMappingResponseDto> deleteUserGroupFunctionMappingListByUserGroupId(@RequestBody DeleteUserGroupFunctionMappingRequestDto request);

	@Operation(summary = "API to get function id list by user group id from database")
	@PostMapping(value = UserPath.GET_FUNCTION_ID_LIST_BY_USER_GROUP_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetFunctionIdResponseDto> getFunctionIdListByUserGroupId(@RequestBody GetFunctionIdRequestDto request);
	
	@Operation(summary = "API to get user group id list by function id from database")
	@PostMapping(value = UserPath.GET_USER_GROUP_ID_LIST_BY_FUNCTION_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetUserGroupIdResponseDto> getUserGroupIdListByFunctionId(@RequestBody GetUserGroupIdRequestDto request);
	
}
