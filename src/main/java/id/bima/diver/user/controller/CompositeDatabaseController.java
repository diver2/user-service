package id.bima.diver.user.controller;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.dto.CompositeDeleteRequestDto;
import id.bima.diver.common.dto.CompositeDeleteResponseDto;
import id.bima.diver.common.dto.GetResponseDto;
import id.bima.diver.common.dto.SaveRequestDto;
import id.bima.diver.common.dto.SaveResponseDto;
import id.bima.diver.common.util.DtoUtil;
import id.bima.diver.user.service.CompositeDatabaseService;

@Component
public abstract class CompositeDatabaseController<
			S extends CompositeDatabaseService<R, U, D, E, I>, 
			R extends JpaRepository<E, I>, 
			U extends DtoUtil<D, E>, 
			D extends Serializable, 
			E extends Serializable, 
			I>
		implements CompositeDatabaseApi<D> {
	
	protected S service;
	
	@Autowired
	protected CompositeDatabaseController(S service) {
		this.service = service;
	}

	@Override
	public BaseResponse<CompositeDeleteResponseDto<D>> delete(@RequestBody CompositeDeleteRequestDto<D> req) {
		return service.delete(req);
	}
	
	@Override
	public BaseResponse<CompositeDeleteResponseDto<D>> deleteList(@RequestBody CompositeDeleteRequestDto<D> req) {
		return service.deleteList(req);
	}
	
	@Override
	public BaseResponse<GetResponseDto<D>> getAll() {
		return service.getAll();
	}
	
	@Override
	public BaseResponse<SaveResponseDto<D>> save(@RequestBody SaveRequestDto<D> req) {
		return service.save(req);
	}
	
	@Override
	public BaseResponse<SaveResponseDto<D>> saveList(@RequestBody SaveRequestDto<D> req) {
		return service.saveList(req);
	}
	
}
