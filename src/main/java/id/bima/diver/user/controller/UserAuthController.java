package id.bima.diver.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.GetUserAuthRequestDto;
import id.bima.diver.user.dto.GetUserAuthResponseDto;
import id.bima.diver.user.dto.UserAuthDto;
import id.bima.diver.user.entity.UserAuth;
import id.bima.diver.user.repository.UserAuthRepository;
import id.bima.diver.user.service.UserAuthService;
import id.bima.diver.user.util.UserAuthDtoUtil;

@RestController
@RequestMapping(UserPath.USER_AUTH_V1)
public class UserAuthController 
		extends DefaultDatabaseController<UserAuthService, UserAuthRepository, UserAuthDtoUtil, UserAuthDto, UserAuth, Long> 
		implements UserAuthApi {

	@Autowired
	protected UserAuthController(UserAuthService service) {
		super(service);
	}

	@Override
	public BaseResponse<GetUserAuthResponseDto> getListByUserId(@RequestBody GetUserAuthRequestDto request) {
		return service.getListByUserId(request);
	}
	
	@Override
	public BaseResponse<GetUserAuthResponseDto> getListByUserIdAndIsDeleted(@RequestBody GetUserAuthRequestDto request) {
		return service.getListByUserIdAndIsDeleted(request);
	}

	@Override
	public BaseResponse<GetUserAuthResponseDto> getListByUserName(GetUserAuthRequestDto request) {
		return service.getListByUserName(request);
	}

	@Override
	public BaseResponse<GetUserAuthResponseDto> getListByUserNameAndIsDeleted(GetUserAuthRequestDto request) {
		return service.getListByUserNameAndIsDeleted(request);
	}
	
}
