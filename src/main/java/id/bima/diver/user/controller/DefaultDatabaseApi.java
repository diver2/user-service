package id.bima.diver.user.controller;

import java.io.Serializable;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.dto.DeleteRequestDto;
import id.bima.diver.common.dto.DeleteResponseDto;
import id.bima.diver.common.dto.GetRequestDto;
import id.bima.diver.common.dto.GetResponseDto;
import id.bima.diver.common.dto.SaveRequestDto;
import id.bima.diver.common.dto.SaveResponseDto;
import id.bima.diver.user.constant.DefaultPath;
import io.swagger.v3.oas.annotations.Operation;

public interface DefaultDatabaseApi<D extends Serializable, I> {

	@Operation(summary = "API to delete data from database")
	@PostMapping(value = DefaultPath.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<DeleteResponseDto<D, I>> deleteById(@RequestBody DeleteRequestDto<D, I> req);
	
	@Operation(summary = "API to delete list data from database")
	@PostMapping(value = DefaultPath.DELETE_LIST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<DeleteResponseDto<D, I>> deleteByIdList(@RequestBody DeleteRequestDto<D, I> req);

	@Operation(summary = "API to get all data from database")
	@PostMapping(value = DefaultPath.GET_ALL, produces = { MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetResponseDto<D>> getAll();
	
	@Operation(summary = "API to get data by id from database")
	@PostMapping(value = DefaultPath.GET_BY_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetResponseDto<D>> getById(@RequestBody GetRequestDto<I> req);
	
	@Operation(summary = "API to get list data by list id from database")
	@PostMapping(value = DefaultPath.GET_LIST_BY_LIST_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetResponseDto<D>> getListByListId(@RequestBody GetRequestDto<I> req);
	
	@Operation(summary = "API to save data from database")
	@PostMapping(value = DefaultPath.SAVE, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<SaveResponseDto<D>> save(@RequestBody SaveRequestDto<D> req);
	
	@Operation(summary = "API to save list data from database")
	@PostMapping(value = DefaultPath.SAVE_LIST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<SaveResponseDto<D>> saveList(@RequestBody SaveRequestDto<D> req);
	
}
