package id.bima.diver.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.DeleteUserGroupMappingRequestDto;
import id.bima.diver.user.dto.DeleteUserGroupMappingResponseDto;
import id.bima.diver.user.dto.GetUserGroupIdRequestDto;
import id.bima.diver.user.dto.GetUserGroupIdResponseDto;
import id.bima.diver.user.dto.GetUserIdRequestDto;
import id.bima.diver.user.dto.GetUserIdResponseDto;
import id.bima.diver.user.dto.UserGroupMappingDto;
import id.bima.diver.user.entity.UserGroupMapping;
import id.bima.diver.user.entity.UserGroupMappingPk;
import id.bima.diver.user.repository.UserGroupMappingRepository;
import id.bima.diver.user.service.UserGroupMappingService;
import id.bima.diver.user.util.UserGroupMappingDtoUtil;

@RestController
@RequestMapping(UserPath.USER_GROUP_MAPPING_V1)
public class UserGroupMappingController 
		extends CompositeDatabaseController<UserGroupMappingService, UserGroupMappingRepository, UserGroupMappingDtoUtil, UserGroupMappingDto, UserGroupMapping, UserGroupMappingPk>
		implements UserGroupMappingApi {
	
	@Autowired
	protected UserGroupMappingController(UserGroupMappingService service) {
		super(service);
	}

	@Override
	public BaseResponse<DeleteUserGroupMappingResponseDto> deleteUserGroupMappingListByUserGroupId(DeleteUserGroupMappingRequestDto request) {
		return service.deleteUserGroupMappingListByUserGroupId(request);
	}

	@Override
	public BaseResponse<DeleteUserGroupMappingResponseDto> deleteUserGroupMappingListByUserId(DeleteUserGroupMappingRequestDto request) {
		return service.deleteUserGroupMappingListByUserId(request);
	}

	@Override
	public BaseResponse<GetUserGroupIdResponseDto> getUserGroupIdListByUserId(@RequestBody GetUserGroupIdRequestDto request) {
		return service.getUserGroupIdListByUserId(request);
	}
	
	@Override
	public BaseResponse<GetUserIdResponseDto> getUserIdListByUserGroupId(@RequestBody GetUserIdRequestDto request) {
		return service.getUserIdListByUserGroupId(request);
	}

}
