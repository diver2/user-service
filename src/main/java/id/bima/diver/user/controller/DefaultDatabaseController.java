package id.bima.diver.user.controller;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.dto.DeleteRequestDto;
import id.bima.diver.common.dto.DeleteResponseDto;
import id.bima.diver.common.dto.GetRequestDto;
import id.bima.diver.common.dto.GetResponseDto;
import id.bima.diver.common.dto.SaveRequestDto;
import id.bima.diver.common.dto.SaveResponseDto;
import id.bima.diver.common.util.DtoUtil;
import id.bima.diver.user.service.DefaultDatabaseService;

@Component
public abstract class DefaultDatabaseController<
			S extends DefaultDatabaseService<R, U, D, E, I>, 
			R extends JpaRepository<E, I>, 
			U extends DtoUtil<D, E>, 
			D extends Serializable, 
			E extends Serializable, 
			I>
		implements DefaultDatabaseApi<D, I> {
	
	protected S service;
	
	@Autowired
	protected DefaultDatabaseController(S service) {
		this.service = service;
	}

	@Override
	public BaseResponse<DeleteResponseDto<D, I>> deleteById(@RequestBody DeleteRequestDto<D, I> req) {
		return service.deleteById(req);
	}
	
	@Override
	public BaseResponse<DeleteResponseDto<D, I>> deleteByIdList(@RequestBody DeleteRequestDto<D, I> req) {
		return service.deleteByIdList(req);
	}
	
	@Override
	public BaseResponse<GetResponseDto<D>> getAll() {
		return service.getAll();
	}
	
	@Override
	public BaseResponse<GetResponseDto<D>> getById(@RequestBody GetRequestDto<I> req) {
		return service.getById(req);
	}
	
	@Override
	public BaseResponse<GetResponseDto<D>> getListByListId(@RequestBody GetRequestDto<I> req) {
		return service.getListByListId(req);
	}
	
	@Override
	public BaseResponse<SaveResponseDto<D>> save(@RequestBody SaveRequestDto<D> req) {
		return service.save(req);
	}
	
	@Override
	public BaseResponse<SaveResponseDto<D>> saveList(@RequestBody SaveRequestDto<D> req) {
		return service.saveList(req);
	}
	
}
