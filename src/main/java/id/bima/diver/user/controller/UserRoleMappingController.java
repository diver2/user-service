package id.bima.diver.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.DeleteUserRoleMappingRequestDto;
import id.bima.diver.user.dto.DeleteUserRoleMappingResponseDto;
import id.bima.diver.user.dto.GetRoleIdRequestDto;
import id.bima.diver.user.dto.GetRoleIdResponseDto;
import id.bima.diver.user.dto.GetUserIdRequestDto;
import id.bima.diver.user.dto.GetUserIdResponseDto;
import id.bima.diver.user.dto.UserRoleMappingDto;
import id.bima.diver.user.entity.UserRoleMapping;
import id.bima.diver.user.entity.UserRoleMappingPk;
import id.bima.diver.user.repository.UserRoleMappingRepository;
import id.bima.diver.user.service.UserRoleMappingService;
import id.bima.diver.user.util.UserRoleMappingDtoUtil;

@RestController
@RequestMapping(UserPath.USER_ROLE_MAPPING_V1)
public class UserRoleMappingController 
		extends CompositeDatabaseController<UserRoleMappingService, UserRoleMappingRepository, UserRoleMappingDtoUtil, UserRoleMappingDto, UserRoleMapping, UserRoleMappingPk>
		implements UserRoleMappingApi {
	
	@Autowired
	protected UserRoleMappingController(UserRoleMappingService service) {
		super(service);
	}

	@Override
	public BaseResponse<DeleteUserRoleMappingResponseDto> deleteUserRoleMappingListByRoleId(DeleteUserRoleMappingRequestDto request) {
		return service.deleteUserRoleMappingListByRoleId(request);
	}

	@Override
	public BaseResponse<DeleteUserRoleMappingResponseDto> deleteUserRoleMappingListByUserId(DeleteUserRoleMappingRequestDto request) {
		return service.deleteUserRoleMappingListByUserId(request);
	}

	@Override
	public BaseResponse<GetRoleIdResponseDto> getRoleIdListByUserId(@RequestBody GetRoleIdRequestDto request) {
		return service.getRoleIdListByUserId(request);
	}
	
	@Override
	public BaseResponse<GetUserIdResponseDto> getUserIdListByRoleId(@RequestBody GetUserIdRequestDto request) {
		return service.getUserIdListByRoleId(request);
	}

}
