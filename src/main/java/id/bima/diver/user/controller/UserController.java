package id.bima.diver.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.GetUserRequestDto;
import id.bima.diver.user.dto.GetUserResponseDto;
import id.bima.diver.user.dto.UserDto;
import id.bima.diver.user.dto.UserGroupResponse;
import id.bima.diver.user.entity.User;
import id.bima.diver.user.repository.UserRepository;
import id.bima.diver.user.service.UserService;
import id.bima.diver.user.util.UserDtoUtil;

@RestController
@RequestMapping(UserPath.USER_V1)
public class UserController 
		extends DefaultDatabaseController<UserService, UserRepository, UserDtoUtil, UserDto, User, Long> 
		implements UserApi {

	@Autowired
	protected UserController(UserService service) {
		super(service);
	}

	@Override
	public BaseResponse<GetUserResponseDto> getUserFullById(GetUserRequestDto request) {
		return service.getUserFullById(request);
	}

	@Override
	public BaseResponse<List<UserGroupResponse>> getAllUserRole() {
		return service.getAllUserDetails();
	}
}
