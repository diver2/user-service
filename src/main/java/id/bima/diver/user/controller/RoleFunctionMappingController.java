package id.bima.diver.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.DeleteRoleFunctionMappingRequestDto;
import id.bima.diver.user.dto.DeleteRoleFunctionMappingResponseDto;
import id.bima.diver.user.dto.GetFunctionIdRequestDto;
import id.bima.diver.user.dto.GetFunctionIdResponseDto;
import id.bima.diver.user.dto.GetRoleIdRequestDto;
import id.bima.diver.user.dto.GetRoleIdResponseDto;
import id.bima.diver.user.dto.RoleFunctionMappingDto;
import id.bima.diver.user.entity.RoleFunctionMapping;
import id.bima.diver.user.entity.RoleFunctionMappingPk;
import id.bima.diver.user.repository.RoleFunctionMappingRepository;
import id.bima.diver.user.service.RoleFunctionMappingService;
import id.bima.diver.user.util.RoleFunctionMappingDtoUtil;

@RestController
@RequestMapping(UserPath.ROLE_FUNCTION_MAPPING_V1)
public class RoleFunctionMappingController 
		extends CompositeDatabaseController<RoleFunctionMappingService, RoleFunctionMappingRepository, RoleFunctionMappingDtoUtil, RoleFunctionMappingDto, RoleFunctionMapping, RoleFunctionMappingPk>
		implements RoleFunctionMappingApi {
	
	@Autowired
	protected RoleFunctionMappingController(RoleFunctionMappingService service) {
		super(service);
	}

	@Override
	public BaseResponse<DeleteRoleFunctionMappingResponseDto> deleteRoleFunctionMappingListByFunctionId(DeleteRoleFunctionMappingRequestDto request) {
		return service.deleteRoleFunctionMappingListByFunctionId(request);
	}

	@Override
	public BaseResponse<DeleteRoleFunctionMappingResponseDto> deleteRoleFunctionMappingListByRoleId(DeleteRoleFunctionMappingRequestDto request) {
		return service.deleteRoleFunctionMappingListByRoleId(request);
	}

	@Override
	public BaseResponse<GetRoleIdResponseDto> getRoleIdListByFunctionId(@RequestBody GetRoleIdRequestDto request) {
		return service.getRoleIdListByFunctionId(request);
	}
	
	@Override
	public BaseResponse<GetFunctionIdResponseDto> getFunctionIdListByRoleId(@RequestBody GetFunctionIdRequestDto request) {
		return service.getFunctionIdListByRoleId(request);
	}

	
	
}
