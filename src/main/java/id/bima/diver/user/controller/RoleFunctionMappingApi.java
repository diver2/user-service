package id.bima.diver.user.controller;

import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.DeleteRoleFunctionMappingRequestDto;
import id.bima.diver.user.dto.DeleteRoleFunctionMappingResponseDto;
import id.bima.diver.user.dto.GetFunctionIdRequestDto;
import id.bima.diver.user.dto.GetFunctionIdResponseDto;
import id.bima.diver.user.dto.GetRoleIdRequestDto;
import id.bima.diver.user.dto.GetRoleIdResponseDto;
import id.bima.diver.user.dto.RoleFunctionMappingDto;
import io.swagger.v3.oas.annotations.Operation;

@Lazy
public interface RoleFunctionMappingApi extends CompositeDatabaseApi<RoleFunctionMappingDto> {
	
	@Operation(summary = "API to delete role function mapping list by function id from database")
	@PostMapping(value = UserPath.DELETE_BY_FUNCTION_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<DeleteRoleFunctionMappingResponseDto> deleteRoleFunctionMappingListByFunctionId(@RequestBody DeleteRoleFunctionMappingRequestDto request);
	
	@Operation(summary = "API to delete role function mapping list by role id from database")
	@PostMapping(value = UserPath.DELETE_BY_ROLE_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<DeleteRoleFunctionMappingResponseDto> deleteRoleFunctionMappingListByRoleId(@RequestBody DeleteRoleFunctionMappingRequestDto request);

	@Operation(summary = "API to get role id list by function id from database")
	@PostMapping(value = UserPath.GET_ROLE_ID_LIST_BY_FUNCTION_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetRoleIdResponseDto> getRoleIdListByFunctionId(@RequestBody GetRoleIdRequestDto request);
	
	@Operation(summary = "API to get function id list by role id from database")
	@PostMapping(value = UserPath.GET_FUNCTION_ID_LIST_BY_ROLE_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetFunctionIdResponseDto> getFunctionIdListByRoleId(@RequestBody GetFunctionIdRequestDto request);
	
}
