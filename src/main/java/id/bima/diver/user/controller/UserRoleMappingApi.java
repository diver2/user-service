package id.bima.diver.user.controller;

import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.user.constant.UserPath;
import id.bima.diver.user.dto.DeleteUserRoleMappingRequestDto;
import id.bima.diver.user.dto.DeleteUserRoleMappingResponseDto;
import id.bima.diver.user.dto.GetRoleIdRequestDto;
import id.bima.diver.user.dto.GetRoleIdResponseDto;
import id.bima.diver.user.dto.GetUserIdRequestDto;
import id.bima.diver.user.dto.GetUserIdResponseDto;
import id.bima.diver.user.dto.UserRoleMappingDto;
import io.swagger.v3.oas.annotations.Operation;

@Lazy
public interface UserRoleMappingApi extends CompositeDatabaseApi<UserRoleMappingDto> {
	
	@Operation(summary = "API to delete user role mapping list by user group id from database")
	@PostMapping(value = UserPath.DELETE_BY_ROLE_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<DeleteUserRoleMappingResponseDto> deleteUserRoleMappingListByRoleId(@RequestBody DeleteUserRoleMappingRequestDto request);

	@Operation(summary = "API to delete user role mapping list by user id from database")
	@PostMapping(value = UserPath.DELETE_BY_USER_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<DeleteUserRoleMappingResponseDto> deleteUserRoleMappingListByUserId(@RequestBody DeleteUserRoleMappingRequestDto request);

	@Operation(summary = "API to get role id list by user id from database")
	@PostMapping(value = UserPath.GET_ROLE_ID_LIST_BY_USER_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetRoleIdResponseDto> getRoleIdListByUserId(@RequestBody GetRoleIdRequestDto request);
	
	@Operation(summary = "API to get user id list by role id from database")
	@PostMapping(value = UserPath.GET_USER_ID_LIST_BY_ROLE_ID, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetUserIdResponseDto> getUserIdListByRoleId(@RequestBody GetUserIdRequestDto request);
	
}
