package id.bima.diver.user.controller;

import java.io.Serializable;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.dto.CompositeDeleteRequestDto;
import id.bima.diver.common.dto.CompositeDeleteResponseDto;
import id.bima.diver.common.dto.GetResponseDto;
import id.bima.diver.common.dto.SaveRequestDto;
import id.bima.diver.common.dto.SaveResponseDto;
import id.bima.diver.user.constant.DefaultPath;
import io.swagger.v3.oas.annotations.Operation;

public interface CompositeDatabaseApi<D extends Serializable> {
	
	@Operation(summary = "API to delete data from database")
	@PostMapping(value = DefaultPath.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<CompositeDeleteResponseDto<D>> delete(@RequestBody CompositeDeleteRequestDto<D> req);
	
	@Operation(summary = "API to delete list data from database")
	@PostMapping(value = DefaultPath.DELETE_LIST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<CompositeDeleteResponseDto<D>> deleteList(@RequestBody CompositeDeleteRequestDto<D> req);

	@Operation(summary = "API to get all data from database")
	@PostMapping(value = DefaultPath.GET_ALL, produces = { MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetResponseDto<D>> getAll();
	
	@Operation(summary = "API to save data from database")
	@PostMapping(value = DefaultPath.SAVE, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<SaveResponseDto<D>> save(@RequestBody SaveRequestDto<D> req);
	
	@Operation(summary = "API to save list data from database")
	@PostMapping(value = DefaultPath.SAVE_LIST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<SaveResponseDto<D>> saveList(@RequestBody SaveRequestDto<D> req);
	
}
