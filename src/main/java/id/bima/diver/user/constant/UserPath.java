package id.bima.diver.user.constant;

public class UserPath {

	private UserPath() {
	}
	
	// Main Path
	public static final String FUNCTION_V1 = "v1/function";
	public static final String ROLE_FUNCTION_MAPPING_V1 = "v1/role-function-mapping";
	public static final String ROLE_V1 = "v1/role";
	public static final String USER_V1 = "v1/user";
	public static final String USER_AUTH_V1 = "v1/user-auth";
	public static final String USER_GROUP_FUNCTION_MAPPING_V1 = "v1/user-group-function-mapping";
	public static final String USER_GROUP_MAPPING_V1 = "v1/user-group-mapping";
	public static final String USER_GROUP_V1 = "v1/user-group";
	public static final String USER_ROLE_MAPPING_V1 = "v1/user-role-mapping";
	
	// Sub Path
	public static final String DELETE_BY_FUNCTION_ID = "/delete-by-function-id";
	public static final String DELETE_BY_ROLE_ID = "/delete-by-role-id";
	public static final String DELETE_BY_USER_GROUP_ID = "/delete-by-user-group-id";
	public static final String DELETE_BY_USER_ID = "/delete-by-user-id";
	public static final String GET_FUNCTION_ID_LIST_BY_ROLE_ID = "/get-function-id-list-by-role-id";
	public static final String GET_FUNCTION_ID_LIST_BY_USER_GROUP_ID = "/get-function-id-list-by-user-group-id";
	public static final String GET_LIST_BY_USER_ID = "/get-list-by-user-id";
	public static final String GET_LIST_BY_USER_ID_AND_IS_DELETED = "/get-list-by-user-id-and-is-deleted";
	public static final String GET_LIST_BY_USER_NAME = "/get-list-by-user-name";
	public static final String GET_LIST_BY_USER_NAME_AND_IS_DELETED = "/get-list-by-user-name-and-is-deleted";
	public static final String GET_ROLE_ID_LIST_BY_FUNCTION_ID = "/get-role-id-list-by-function-id";
	public static final String GET_ROLE_ID_LIST_BY_USER_ID = "/get-role-id-list-by-user-id";
	public static final String GET_USER_FULL = "/get-user-full";
	public static final String GET_USER_GROUP_ID_LIST_BY_FUNCTION_ID = "/get-user-group-id-list-by-function-id";
	public static final String GET_USER_GROUP_ID_LIST_BY_USER_ID = "/get-user-group-id-list-by-user-id";
	public static final String GET_USER_ID_LIST_BY_ROLE_ID = "/get-user-id-list-by-role-id";
	public static final String GET_USER_ID_LIST_BY_USER_GROUP_ID = "/get-user-id-list-by-user-group-id";

	public static final String GET_ALL_USER_ROLE = "/get-user-role-all";

}
