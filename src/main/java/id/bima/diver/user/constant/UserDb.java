package id.bima.diver.user.constant;

public class UserDb {

	private UserDb() {
	}

	// Schema name
	public static final String SCHEMA = "user_mgt";

	// Table name
	public static final String FUNCTION = "m_function";
	public static final String ROLE = "m_role";
	public static final String ROLE_FUNCTION_MAPPING = "m_role_function_mapping";
	public static final String USER = "m_user";
	public static final String USER_AUTH = "m_user_auth";
	public static final String USER_GROUP = "m_user_group";
	public static final String USER_GROUP_FUNCTION_MAPPING = "m_user_group_function_mapping";
	public static final String USER_GROUP_MAPPING = "m_user_group_mapping";
	public static final String USER_ROLE_MAPPING = "m_user_role_mapping";
	

}
