package id.bima.diver.user.constant;

public class DefaultPath {

	private DefaultPath() {
	}
	
	// Sub Path
	public static final String DELETE = "/delete";
	public static final String DELETE_LIST = "/delete-list";
	public static final String GET_ALL = "/get-all";
	public static final String GET_BY_ID = "/get-by-id";
	public static final String GET_LIST_BY_LIST_ID = "/get-list-by-list_id";
	public static final String SAVE = "/save";
	public static final String SAVE_LIST = "/save-list";
	
}
