package id.bima.diver.user.parameter.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParameterDto implements Serializable {
	
	private static final long serialVersionUID = -7075050125913841009L;
	
	private Long id;
	private String module;
	private String key;
	private String value1;
	private String value2;
	private String value3;
	private String value4;
	private String value5;
	private String value6;
	private String value7;
	private Boolean isEncrypted;
	
}
