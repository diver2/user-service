package id.bima.diver.user.parameter.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetParameterRequestDto implements Serializable {
	
	private static final long serialVersionUID = -4316228949326794208L;
	
	private List<String> listModule;
	private String module;
	private String key;
	
}
