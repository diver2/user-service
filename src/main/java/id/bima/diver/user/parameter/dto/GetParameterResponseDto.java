package id.bima.diver.user.parameter.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetParameterResponseDto implements Serializable {
	
	private static final long serialVersionUID = 308434434781022421L;
	
	private ParameterDto parameter;
	private Map<String, List<ParameterDto>> mapParameter;
	
}
