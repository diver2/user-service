package id.bima.diver.user.parameter.constant;

public class ParameterPath {

	private ParameterPath() {
	}
	
	// Main Path
	public static final String PARAMETER_V1 = "v1/parameter";
	
	// Parameter Controller Sub-Path
	public static final String GET_BY_MODULE_AND_KEY = "/get-by-module-and-key";
	
}
