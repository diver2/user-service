package id.bima.diver.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.bima.diver.user.entity.UserAuth;

@Repository
public interface UserAuthRepository extends JpaRepository<UserAuth, Long> {
	
	public List<UserAuth> findByUserId(Long userId);
	public List<UserAuth> findByUserIdAndIsDeleted(Long userId, Boolean isDeleted);
	public List<UserAuth> findByUserName(String userName);
	public List<UserAuth> findByUserNameAndIsDeleted(String userName, Boolean isDeleted);
	
}
