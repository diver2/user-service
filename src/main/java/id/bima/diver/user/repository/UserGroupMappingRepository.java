package id.bima.diver.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.bima.diver.user.dto.UserGroupId;
import id.bima.diver.user.dto.UserId;
import id.bima.diver.user.entity.UserGroupMapping;
import id.bima.diver.user.entity.UserGroupMappingPk;

public interface UserGroupMappingRepository extends JpaRepository<UserGroupMapping, UserGroupMappingPk> {

	public List<UserGroupMapping> deleteByIdUserGroupId(Long userGroupId);
	public List<UserGroupMapping> deleteByIdUserId(Long userId);
	
	public List<UserGroupId> findIdUserGroupIdByIdUserId(Long userId);
    public List<UserId> findIdUserIdByIdUserGroupId(Long userGroupId);
    
}
