package id.bima.diver.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.bima.diver.user.dto.FunctionId;
import id.bima.diver.user.dto.RoleId;
import id.bima.diver.user.entity.RoleFunctionMapping;
import id.bima.diver.user.entity.RoleFunctionMappingPk;

public interface RoleFunctionMappingRepository extends JpaRepository<RoleFunctionMapping, RoleFunctionMappingPk> {
	
	public List<RoleFunctionMapping> deleteByIdFunctionId(Long functionId);
	public List<RoleFunctionMapping> deleteByIdRoleId(Long roleId);

	public List<RoleId> findIdRoleIdByIdFunctionId(Long functionId);
    public List<FunctionId> findIdFunctionIdByIdRoleId(Long roleId);
    public List<FunctionId> findIdFunctionIdByIdRoleIdIn(List<Long> list);
	
}
