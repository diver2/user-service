package id.bima.diver.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.bima.diver.user.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	
}
