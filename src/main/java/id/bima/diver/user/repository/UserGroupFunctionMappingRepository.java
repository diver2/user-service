package id.bima.diver.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.bima.diver.user.dto.FunctionId;
import id.bima.diver.user.dto.UserGroupId;
import id.bima.diver.user.entity.UserGroupFunctionMapping;
import id.bima.diver.user.entity.UserGroupFunctionMappingPk;

public interface UserGroupFunctionMappingRepository extends JpaRepository<UserGroupFunctionMapping, UserGroupFunctionMappingPk> {

	public List<UserGroupFunctionMapping> deleteByIdFunctionId(Long functionId);
	public List<UserGroupFunctionMapping> deleteByIdUserGroupId(Long userGroupId);
	
	public List<FunctionId> findIdFunctionIdByIdUserGroupId(Long userGroupId);
	public List<FunctionId> findIdFunctionIdByIdUserGroupIdIn(List<Long> list);
	public List<UserGroupId> findIdUserGroupIdByIdFunctionId(Long functionId);

}
