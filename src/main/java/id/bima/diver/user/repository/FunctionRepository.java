package id.bima.diver.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.bima.diver.user.entity.Function;

@Repository
public interface FunctionRepository extends JpaRepository<Function, Long> {
	
}
