package id.bima.diver.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.bima.diver.user.dto.RoleId;
import id.bima.diver.user.dto.UserId;
import id.bima.diver.user.entity.UserRoleMapping;
import id.bima.diver.user.entity.UserRoleMappingPk;

public interface UserRoleMappingRepository extends JpaRepository<UserRoleMapping, UserRoleMappingPk> {

	public List<UserRoleMapping> deleteByIdRoleId(Long roleId);
	public List<UserRoleMapping> deleteByIdUserId(Long userId);
	
	public List<RoleId> findIdRoleIdByIdUserId(Long userId);
    public List<UserId> findIdUserIdByIdRoleId(Long roleId);
	
}
