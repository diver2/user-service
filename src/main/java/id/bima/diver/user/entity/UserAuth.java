package id.bima.diver.user.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import id.bima.diver.user.constant.UserDb;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = UserDb.USER_AUTH, schema = UserDb.SCHEMA)
@DynamicInsert
@DynamicUpdate
public class UserAuth extends AuditEntity {

	private static final long serialVersionUID = 1589236428401323387L;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(name = "is_deleted")
	private Boolean isDeleted;

	@Column(name = "user_id")
	private Long userId;

}
