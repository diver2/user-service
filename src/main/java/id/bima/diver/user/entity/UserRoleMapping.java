package id.bima.diver.user.entity;

import java.io.Serializable;

import id.bima.diver.user.constant.UserDb;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = UserDb.USER_ROLE_MAPPING, schema = UserDb.SCHEMA)
public class UserRoleMapping implements Serializable {

	private static final long serialVersionUID = 3674547613545388066L;

	@EmbeddedId
	private UserRoleMappingPk id;

}
