package id.bima.diver.user.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import id.bima.diver.user.constant.UserDb;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = UserDb.USER_GROUP, schema = UserDb.SCHEMA)
@DynamicInsert
@DynamicUpdate
public class UserGroup extends AuditEntity {

	private static final long serialVersionUID = -6322620527762339334L;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

}
