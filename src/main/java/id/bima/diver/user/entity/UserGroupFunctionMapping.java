package id.bima.diver.user.entity;

import java.io.Serializable;

import id.bima.diver.user.constant.UserDb;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = UserDb.USER_GROUP_FUNCTION_MAPPING, schema = UserDb.SCHEMA)
public class UserGroupFunctionMapping implements Serializable {

	private static final long serialVersionUID = 5425947500173329312L;

	@EmbeddedId
	private UserGroupFunctionMappingPk id;

}
