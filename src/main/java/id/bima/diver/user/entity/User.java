package id.bima.diver.user.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import id.bima.diver.user.constant.UserDb;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = UserDb.USER, schema = UserDb.SCHEMA)
@DynamicInsert
@DynamicUpdate
public class User extends AuditEntity {

	private static final long serialVersionUID = 1111785836710661712L;

	@Column(name = "nick_name")
	private String nickName;

	@Column(name = "full_name")
	private String fullName;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "email")
	private String email;

	@Column(name = "status")
	private Integer status;

	@Column(name = "superior_id")
	private Long superiorId;

}
