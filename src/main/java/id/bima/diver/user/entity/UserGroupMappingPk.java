package id.bima.diver.user.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class UserGroupMappingPk implements Serializable {

	private static final long serialVersionUID = 5194217357123113434L;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "user_group_id")
	private Long userGroupId;

}
