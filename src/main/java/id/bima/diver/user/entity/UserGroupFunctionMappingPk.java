package id.bima.diver.user.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class UserGroupFunctionMappingPk implements Serializable {

	private static final long serialVersionUID = -7240415095599336227L;

	@Column(name = "user_group_id")
	private Long userGroupId;

	@Column(name = "function_id")
	private Long functionId;

}
