package id.bima.diver.user.entity;

import java.io.Serializable;

import id.bima.diver.user.constant.UserDb;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = UserDb.ROLE_FUNCTION_MAPPING, schema = UserDb.SCHEMA)
public class RoleFunctionMapping implements Serializable {

	private static final long serialVersionUID = 1677988143053982576L;

	@EmbeddedId
	private RoleFunctionMappingPk id;

}
