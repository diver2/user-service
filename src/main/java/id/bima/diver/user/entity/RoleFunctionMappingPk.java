package id.bima.diver.user.entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class RoleFunctionMappingPk implements Serializable {

	private static final long serialVersionUID = 6573711780579669791L;

	@Column(name = "role_id")
	private Long roleId;

	@Column(name = "function_id")
	private Long functionId;

}
