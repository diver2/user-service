package id.bima.diver.user.entity;

import java.io.Serializable;

import id.bima.diver.user.constant.UserDb;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = UserDb.USER_GROUP_MAPPING, schema = UserDb.SCHEMA)
public class UserGroupMapping implements Serializable {

	private static final long serialVersionUID = 6099259164425864754L;

	@EmbeddedId
	private UserGroupMappingPk id;

}
