package id.bima.diver.user.util;

import org.springframework.stereotype.Component;

import id.bima.diver.common.util.DtoUtil;
import id.bima.diver.user.dto.UserGroupFunctionMappingDto;
import id.bima.diver.user.entity.UserGroupFunctionMapping;
import id.bima.diver.user.entity.UserGroupFunctionMappingPk;

@Component
public class UserGroupFunctionMappingDtoUtil implements DtoUtil<UserGroupFunctionMappingDto, UserGroupFunctionMapping> {

	@Override
	public UserGroupFunctionMapping constructEntity(UserGroupFunctionMappingDto dto) {
		if (dto == null) {
			return null;
		}
		
		return UserGroupFunctionMapping.builder()
				.id(UserGroupFunctionMappingPk.builder()
						.userGroupId(dto.getUserGroupId())
						.functionId(dto.getFunctionId())
						.build())
				.build();
	}

	@Override
	public UserGroupFunctionMappingDto constructDto(UserGroupFunctionMapping entity) {
		if (entity == null) {
			return null;
		}
		
		return UserGroupFunctionMappingDto.builder()
				.userGroupId(entity.getId().getUserGroupId())
				.functionId(entity.getId().getFunctionId())
				.build();
	}

}
