package id.bima.diver.user.util;

import org.springframework.stereotype.Component;

import id.bima.diver.common.util.DtoUtil;
import id.bima.diver.user.dto.UserGroupMappingDto;
import id.bima.diver.user.entity.UserGroupMapping;
import id.bima.diver.user.entity.UserGroupMappingPk;

@Component
public class UserGroupMappingDtoUtil implements DtoUtil<UserGroupMappingDto, UserGroupMapping> {

	@Override
	public UserGroupMapping constructEntity(UserGroupMappingDto dto) {
		if (dto == null) {
			return null;
		}
		
		return UserGroupMapping.builder()
				.id(UserGroupMappingPk.builder()
						.userId(dto.getUserId())
						.userGroupId(dto.getUserGroupId())
						.build())
				.build();
	}

	@Override
	public UserGroupMappingDto constructDto(UserGroupMapping entity) {
		if (entity == null) {
			return null;
		}
		
		return UserGroupMappingDto.builder()
				.userId(entity.getId().getUserId())
				.userGroupId(entity.getId().getUserGroupId())
				.build();
	}

}
