package id.bima.diver.user.util;

import org.springframework.stereotype.Component;

import id.bima.diver.common.util.DtoUtil;
import id.bima.diver.user.dto.UserAuthDto;
import id.bima.diver.user.entity.UserAuth;

@Component
public class UserAuthDtoUtil implements DtoUtil<UserAuthDto, UserAuth> {

	public UserAuth constructEntity(UserAuthDto dto) {
		if (dto == null) {
			return null;
		}
		
		return UserAuth.builder()
				.id(dto.getId())
				.userName(dto.getUserName())
				.password(dto.getPassword())
				.isDeleted(dto.getIsDeleted())
				.userId(dto.getUserId())
				.build();
	}
	
	public UserAuthDto constructDto(UserAuth entity) {
		if (entity == null) {
			return null;
		}
		
		return UserAuthDto.builder()
				.id(entity.getId())
				.userName(entity.getUserName())
				.password(entity.getPassword())
				.isDeleted(entity.getIsDeleted())
				.userId(entity.getUserId())
				.build();
	}
	
}
