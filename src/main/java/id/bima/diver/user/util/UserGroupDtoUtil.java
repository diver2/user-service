package id.bima.diver.user.util;

import org.springframework.stereotype.Component;

import id.bima.diver.common.util.DtoUtil;
import id.bima.diver.user.dto.UserGroupDto;
import id.bima.diver.user.entity.UserGroup;

@Component
public class UserGroupDtoUtil implements DtoUtil<UserGroupDto, UserGroup> {
	
	public UserGroup constructEntity(UserGroupDto dto) {
		if (dto == null) {
			return null;
		}
		
		return UserGroup.builder()
				.id(dto.getId())
				.name(dto.getName())
				.description(dto.getDescription())
				.build();
	}

	public UserGroupDto constructDto(UserGroup entity) {
		if (entity == null) {
			return null;
		}
		
		return UserGroupDto.builder()
				.id(entity.getId())
				.name(entity.getName())
				.description(entity.getDescription())
				.build();
	}
	
	
}
