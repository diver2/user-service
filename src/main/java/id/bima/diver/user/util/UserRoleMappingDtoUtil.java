package id.bima.diver.user.util;

import org.springframework.stereotype.Component;

import id.bima.diver.common.util.DtoUtil;
import id.bima.diver.user.dto.UserRoleMappingDto;
import id.bima.diver.user.entity.UserRoleMapping;
import id.bima.diver.user.entity.UserRoleMappingPk;

@Component
public class UserRoleMappingDtoUtil implements DtoUtil<UserRoleMappingDto, UserRoleMapping> {

	@Override
	public UserRoleMapping constructEntity(UserRoleMappingDto dto) {
		if (dto == null) {
			return null;
		}
		
		return UserRoleMapping.builder()
				.id(UserRoleMappingPk.builder()
						.userId(dto.getUserId())
						.roleId(dto.getRoleId())
						.build())
				.build();
	}

	@Override
	public UserRoleMappingDto constructDto(UserRoleMapping entity) {
		if (entity == null) {
			return null;
		}
		
		return UserRoleMappingDto.builder()
				.userId(entity.getId().getUserId())
				.roleId(entity.getId().getRoleId())
				.build();
	}

}
