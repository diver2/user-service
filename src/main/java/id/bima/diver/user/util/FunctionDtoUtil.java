package id.bima.diver.user.util;

import org.springframework.stereotype.Component;

import id.bima.diver.common.util.DtoUtil;
import id.bima.diver.user.dto.FunctionDto;
import id.bima.diver.user.entity.Function;

@Component
public class FunctionDtoUtil implements DtoUtil<FunctionDto, Function> {

	public FunctionDto constructDto(Function entity) {
		if (entity == null) {
			return null;
		}
		
		return FunctionDto.builder()
				.id(entity.getId())
				.name(entity.getName())
				.description(entity.getDescription())
				.build();
	}
	
	public Function constructEntity(FunctionDto dto) {
		if (dto == null) {
			return null;
		}
		
		return Function.builder()
				.id(dto.getId())
				.name(dto.getName())
				.description(dto.getDescription())
				.build();
	}
	
}
