package id.bima.diver.user.util;

import org.springframework.stereotype.Component;

import id.bima.diver.common.util.DtoUtil;
import id.bima.diver.user.dto.RoleDto;
import id.bima.diver.user.entity.Role;

@Component
public class RoleDtoUtil implements DtoUtil<RoleDto, Role> {
	
	public Role constructEntity(RoleDto dto) {
		if (dto == null) {
			return null;
		}
		
		return Role.builder()
				.id(dto.getId())
				.name(dto.getName())
				.description(dto.getDescription())
				.build();
	}

	public RoleDto constructDto(Role entity) {
		if (entity == null) {
			return null;
		}
		
		return RoleDto.builder()
				.id(entity.getId())
				.name(entity.getName())
				.description(entity.getDescription())
				.build();
	}
	
}
