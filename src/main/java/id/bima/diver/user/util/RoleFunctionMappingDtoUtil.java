package id.bima.diver.user.util;

import org.springframework.stereotype.Component;

import id.bima.diver.common.util.DtoUtil;
import id.bima.diver.user.dto.RoleFunctionMappingDto;
import id.bima.diver.user.entity.RoleFunctionMapping;
import id.bima.diver.user.entity.RoleFunctionMappingPk;

@Component
public class RoleFunctionMappingDtoUtil implements DtoUtil<RoleFunctionMappingDto, RoleFunctionMapping> {

	@Override
	public RoleFunctionMapping constructEntity(RoleFunctionMappingDto dto) {
		if (dto == null) {
			return null;
		}
		
		return RoleFunctionMapping.builder()
				.id(RoleFunctionMappingPk.builder()
						.roleId(dto.getRoleId())
						.functionId(dto.getFunctionId())
						.build())
				.build();
	}

	@Override
	public RoleFunctionMappingDto constructDto(RoleFunctionMapping entity) {
		if (entity == null) {
			return null;
		}
		
		return RoleFunctionMappingDto.builder()
				.roleId(entity.getId().getRoleId())
				.functionId(entity.getId().getFunctionId())
				.build();
	}

}
