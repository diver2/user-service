package id.bima.diver.user.util;

import org.springframework.stereotype.Component;

import id.bima.diver.common.util.DtoUtil;
import id.bima.diver.user.dto.UserDto;
import id.bima.diver.user.entity.User;

@Component
public class UserDtoUtil implements DtoUtil<UserDto, User> {

	public User constructEntity(UserDto dto) {
		if (dto == null) {
			return null;
		}
		
		return User.builder()
				.id(dto.getId())
				.nickName(dto.getNickName())
				.fullName(dto.getFullName())
				.phoneNumber(dto.getPhoneNumber())
				.email(dto.getEmail())
				.status(dto.getStatus())
				.superiorId(dto.getSuperiorId())
				.build();
	}
	
	public UserDto constructDto(User entity) {
		if (entity == null) {
			return null;
		}
		
		return UserDto.builder()
				.id(entity.getId())
				.nickName(entity.getNickName())
				.fullName(entity.getFullName())
				.phoneNumber(entity.getPhoneNumber())
				.email(entity.getEmail())
				.status(entity.getStatus())
				.superiorId(entity.getSuperiorId())
				.build();
	}

}
