package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetUserIdRequestDto implements Serializable {
	
	private static final long serialVersionUID = -5891253748392972671L;
	
	private Long roleId;
	private Long userGroupId;

}
