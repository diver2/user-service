package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetRoleIdRequestDto implements Serializable {
	
	private static final long serialVersionUID = 2580584915704829262L;
	
	private Long functionId;
	private Long userId;

}
