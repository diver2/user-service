package id.bima.diver.user.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetUserIdResponseDto implements Serializable {
	
	private static final long serialVersionUID = -5829302540376408961L;
	
	private List<UserId> listUserId;

}
