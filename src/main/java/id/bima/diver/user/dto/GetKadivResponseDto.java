package id.bima.diver.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetKadivResponseDto {
    private UserDto user;
    private List<RoleDto> listRole;
    private List<UserGroupDto> listUserGroup;
}
