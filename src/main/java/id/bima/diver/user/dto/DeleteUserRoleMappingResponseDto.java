package id.bima.diver.user.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeleteUserRoleMappingResponseDto implements Serializable {
	
	private static final long serialVersionUID = -7578142451481183697L;
	
	private List<UserRoleMappingDto> listUserRoleMappingDto;

}
