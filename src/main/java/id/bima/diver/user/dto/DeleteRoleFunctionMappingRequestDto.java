package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeleteRoleFunctionMappingRequestDto implements Serializable {
	
	private static final long serialVersionUID = 1403378886682605316L;
	
	private Long functionId;
	private Long roleId;

}
