package id.bima.diver.user.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeleteUserGroupMappingResponseDto implements Serializable {
	
	private static final long serialVersionUID = -3404550831523150542L;
	
	private List<UserGroupMappingDto> listUserGroupMappingDto;

}
