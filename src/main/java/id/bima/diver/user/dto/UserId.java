package id.bima.diver.user.dto;

import java.io.Serializable;

public interface UserId extends Serializable {
	
	public Long getIdUserId();

}
