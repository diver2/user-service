package id.bima.diver.user.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class  GetRoleIdResponseDto implements Serializable {
	
	private static final long serialVersionUID = -3483314725619327301L;
	
	private List<RoleId> listRoleId;

}
