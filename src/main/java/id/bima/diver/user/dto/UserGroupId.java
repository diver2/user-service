package id.bima.diver.user.dto;

import java.io.Serializable;

public interface UserGroupId extends Serializable {
	
	public Long getIdUserGroupId();

}
