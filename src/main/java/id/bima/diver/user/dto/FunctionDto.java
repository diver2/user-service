package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FunctionDto implements Serializable {

	private static final long serialVersionUID = 6885907199054094666L;
	
	private Long id;
	private String name;
	private String description;
	private String value;
	
}
