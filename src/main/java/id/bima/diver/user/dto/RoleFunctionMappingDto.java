package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleFunctionMappingDto implements Serializable {

	private static final long serialVersionUID = -926854938887158536L;
	
	private Long roleId;
	private Long functionId;
	
}
