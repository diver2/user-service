package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetFunctionIdRequestDto implements Serializable {
	
	private static final long serialVersionUID = 6222074338506589058L;
	
	private Long roleId;
	private Long userGroupId;

}
