package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements Serializable {
	
	private static final long serialVersionUID = -8110062732060480559L;
	
	private Long id;
	private String nickName;
	private String fullName;
	private String phoneNumber;
	private String email;
	private Integer status;
	private Long superiorId;
	
}
