package id.bima.diver.user.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetUserGroupIdResponseDto implements Serializable {
	
	private static final long serialVersionUID = 6052838401560710869L;
	
	private List<UserGroupId> listUserGroupId;

}
