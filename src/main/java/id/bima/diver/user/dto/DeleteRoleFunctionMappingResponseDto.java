package id.bima.diver.user.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeleteRoleFunctionMappingResponseDto implements Serializable {
	
	private static final long serialVersionUID = 4955915766891175233L;
	
	private List<RoleFunctionMappingDto> listRoleFunctionMappingDto;

}
