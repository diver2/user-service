package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetUserAuthRequestDto implements Serializable {
	
	private static final long serialVersionUID = -2676711794707396716L;
	
	private Boolean isDeleted;
	private Long userId;
	private String userName;
	
}
