package id.bima.diver.user.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeleteUserGroupFunctionMappingResponseDto implements Serializable {
	
	private static final long serialVersionUID = -420446750679782838L;
	
	private List<UserGroupFunctionMappingDto> listUserGroupFunctionMappingDto;

}
