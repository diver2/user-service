package id.bima.diver.user.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserGroupResponse {
    private Long id;
    private String fullName;
    private List<RoleDto> roles;
    private List<UserGroupDto> userGroups;
}
