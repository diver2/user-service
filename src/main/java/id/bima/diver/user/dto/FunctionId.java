package id.bima.diver.user.dto;

import java.io.Serializable;

public interface FunctionId extends Serializable {
	
	public Long getIdFunctionId();

}
