package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeleteUserGroupMappingRequestDto implements Serializable {
	
	private static final long serialVersionUID = -2406144895300374049L;
	
	private Long userGroupId;
	private Long userId;

}
