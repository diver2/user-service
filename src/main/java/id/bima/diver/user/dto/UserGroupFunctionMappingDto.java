package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserGroupFunctionMappingDto implements Serializable {

	private static final long serialVersionUID = 7673254019998380761L;
	
	private Long userGroupId;
	private Long functionId;
	
}
