package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetUserRequestDto implements Serializable {
	
	private static final long serialVersionUID = 5901360604412320352L;
	
	private Long id;

}
