package id.bima.diver.user.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetFunctionIdResponseDto implements Serializable {
	
	private static final long serialVersionUID = -4932961608930963840L;
	
	private List<FunctionId> listFunctionId;

}
