package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserGroupMappingDto implements Serializable {

	private static final long serialVersionUID = 7335100883952095934L;
	
	private Long userId;
	private Long userGroupId;
	
}
