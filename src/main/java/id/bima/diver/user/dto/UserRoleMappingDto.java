package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRoleMappingDto implements Serializable {

	private static final long serialVersionUID = 8448978474689648L;
	
	private Long userId;
	private Long roleId;
	
}
