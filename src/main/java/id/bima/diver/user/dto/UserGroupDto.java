package id.bima.diver.user.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserGroupDto implements Serializable {

	private static final long serialVersionUID = 2142579578903806432L;
	
	private Long id;
	private String name;
	private String description;
	
}
