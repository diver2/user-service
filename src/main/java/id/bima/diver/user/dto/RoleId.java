package id.bima.diver.user.dto;

import java.io.Serializable;

public interface RoleId extends Serializable {
	
	public Long getIdRoleId();

}
