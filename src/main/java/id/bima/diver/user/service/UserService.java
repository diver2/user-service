package id.bima.diver.user.service;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.constant.GeneralParameterDefault;
import id.bima.diver.common.constant.RedisKey;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.exception.GenericException;
import id.bima.diver.common.util.BaseResponseUtils;
import id.bima.diver.user.dto.FunctionId;
import id.bima.diver.user.dto.GetUserRequestDto;
import id.bima.diver.user.dto.GetUserResponseDto;
import id.bima.diver.user.dto.RoleDto;
import id.bima.diver.user.dto.RoleId;
import id.bima.diver.user.dto.UserDto;
import id.bima.diver.user.dto.UserGroupDto;
import id.bima.diver.user.dto.UserGroupId;
import id.bima.diver.user.dto.UserGroupResponse;
import id.bima.diver.user.entity.Function;
import id.bima.diver.user.entity.Role;
import id.bima.diver.user.entity.User;
import id.bima.diver.user.entity.UserAuth;
import id.bima.diver.user.entity.UserGroup;
import id.bima.diver.user.parameter.dto.ParameterDto;
import id.bima.diver.user.parameter.service.ParameterService;
import id.bima.diver.user.repository.FunctionRepository;
import id.bima.diver.user.repository.RoleFunctionMappingRepository;
import id.bima.diver.user.repository.RoleRepository;
import id.bima.diver.user.repository.UserAuthRepository;
import id.bima.diver.user.repository.UserGroupFunctionMappingRepository;
import id.bima.diver.user.repository.UserGroupMappingRepository;
import id.bima.diver.user.repository.UserGroupRepository;
import id.bima.diver.user.repository.UserRepository;
import id.bima.diver.user.repository.UserRoleMappingRepository;
import id.bima.diver.user.util.FunctionDtoUtil;
import id.bima.diver.user.util.RoleDtoUtil;
import id.bima.diver.user.util.UserAuthDtoUtil;
import id.bima.diver.user.util.UserDtoUtil;
import id.bima.diver.user.util.UserGroupDtoUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserService extends DefaultDatabaseService<UserRepository, UserDtoUtil, UserDto, User, Long> {

    private FunctionDtoUtil functionDtoUtil;
    private RoleDtoUtil roleDtoUtil;
    private UserAuthDtoUtil userAuthDtoUtil;
    private UserDtoUtil userDtoUtil;
    private UserGroupDtoUtil userGroupDtoUtil;
    private FunctionRepository functionRepository;
    private RoleRepository roleRepository;
    private UserAuthRepository userAuthRepository;
    private UserGroupRepository userGroupRepository;
    private UserRepository userRepository;
    private RoleFunctionMappingRepository roleFunctionMappingRepository;
    private UserGroupFunctionMappingRepository userGroupFunctionMappingRepository;
    private UserGroupMappingRepository userGroupMappingRepository;
    private UserRoleMappingRepository userRoleMappingRepository;
    private ParameterService parameterService;
    private RedisService redisService;

    @Autowired
    public UserService(UserRepository repository, UserDtoUtil util, FunctionDtoUtil functionDtoUtil,
			RoleDtoUtil roleDtoUtil, UserAuthDtoUtil userAuthDtoUtil, UserDtoUtil userDtoUtil,
			UserGroupDtoUtil userGroupDtoUtil, FunctionRepository functionRepository, RoleRepository roleRepository,
			UserAuthRepository userAuthRepository, UserGroupRepository userGroupRepository,
			UserRepository userRepository, RoleFunctionMappingRepository roleFunctionMappingRepository,
			UserGroupFunctionMappingRepository userGroupFunctionMappingRepository,
			UserGroupMappingRepository userGroupMappingRepository, UserRoleMappingRepository userRoleMappingRepository,
			ParameterService parameterService, RedisService redisService) {
		super(repository, util);
		this.functionDtoUtil = functionDtoUtil;
		this.roleDtoUtil = roleDtoUtil;
		this.userAuthDtoUtil = userAuthDtoUtil;
		this.userDtoUtil = userDtoUtil;
		this.userGroupDtoUtil = userGroupDtoUtil;
		this.functionRepository = functionRepository;
		this.roleRepository = roleRepository;
		this.userAuthRepository = userAuthRepository;
		this.userGroupRepository = userGroupRepository;
		this.userRepository = userRepository;
		this.roleFunctionMappingRepository = roleFunctionMappingRepository;
		this.userGroupFunctionMappingRepository = userGroupFunctionMappingRepository;
		this.userGroupMappingRepository = userGroupMappingRepository;
		this.userRoleMappingRepository = userRoleMappingRepository;
		this.parameterService = parameterService;
		this.redisService = redisService;
	}

	public BaseResponse<GetUserResponseDto> getUserFullById(GetUserRequestDto req) {
        ErrorEnum rc = ErrorEnum.SUCCESS;
        GetUserResponseDto result = null;
        try {
            log.info("Get user full data by user id started");

            // Get from redis
            ParameterDto expiredParam = parameterService.getByModuleAndKey(RedisKey.GENERAL, GeneralParameterDefault.EXPIRED_CACHE.name());
            Long expired = Long.valueOf(expiredParam.getValue1());
            result = redisService.getDataFromJsonString(RedisKey.USER_DATA + ":" + req.getId(), GetUserResponseDto.class, Duration.ofMinutes(expired));
            if (result == null) {
                // Get user data
                Optional<User> optUser = userRepository.findById(req.getId());
                if (optUser.isEmpty()) {
                    throw new GenericException(ErrorEnum.DATA_NOT_FOUND, "Data user not found");
                }

                // Get user auth
                List<UserAuth> listUserAuth = userAuthRepository.findByUserIdAndIsDeleted(req.getId(), false);

                // Get user roles
                List<Long> listRoleIds = userRoleMappingRepository.findIdRoleIdByIdUserId(req.getId())
                        .stream().filter(Objects::nonNull).map(RoleId::getIdRoleId).toList();
                List<Role> listRole = roleRepository.findAllById(listRoleIds);

                // Get user group
                List<Long> listGroupIds = userGroupMappingRepository.findIdUserGroupIdByIdUserId(req.getId())
                        .stream().filter(Objects::nonNull).map(UserGroupId::getIdUserGroupId).toList();
                List<UserGroup> listUserGroup = userGroupRepository.findAllById(listGroupIds);

                // Get function
                List<Long> listFunctionIdByRoles = roleFunctionMappingRepository.findIdFunctionIdByIdRoleIdIn(listRoleIds)
                        .stream().filter(Objects::nonNull).map(FunctionId::getIdFunctionId).toList();
                List<Long> listFunctionIdByGroups = userGroupFunctionMappingRepository.findIdFunctionIdByIdUserGroupIdIn(listGroupIds)
                        .stream().filter(Objects::nonNull).map(FunctionId::getIdFunctionId).toList();
                List<Long> listFunctionId = Stream.concat(listFunctionIdByRoles.stream(), listFunctionIdByGroups.stream())
                        .distinct().toList();
                List<Function> listFunction = functionRepository.findAllById(listFunctionId);

                result = GetUserResponseDto.builder()
                        .user(userDtoUtil.constructDto(optUser.get()))
                        .userAuth(!CollectionUtils.isEmpty(listUserAuth) ? userAuthDtoUtil.constructDto(listUserAuth.get(0)) : null)
                        .listRole(roleDtoUtil.constructDtoList(listRole))
                        .listUserGroup(userGroupDtoUtil.constructDtoList(listUserGroup))
                        .listFunction(functionDtoUtil.constructDtoList(listFunction))
                        .build();

                // Save to redis
                redisService.setDataAsJsonString(RedisKey.USER_DATA + ":" + req.getId(), result, Duration.ofMinutes(expired));
            }

            log.info("Get user full data by user id finished");
        } catch (Throwable e) {
            log.error("Error when get user full data by user id", e);
            rc = BaseResponseUtils.getErrorCode(e);
        }

        return BaseResponseUtils.constructResponse(rc, result);
    }

    public BaseResponse<List<UserGroupResponse>> getAllUserDetails() {
        ErrorEnum rc = ErrorEnum.SUCCESS;
        List<UserGroupResponse> results = new ArrayList<>();
        try {
            log.info("Get full data for all users started");

            List<User> users = userRepository.findAll();
            for (User user : users) {
                // Get user roles
                List<Long> listRoleIds = userRoleMappingRepository.findIdRoleIdByIdUserId(user.getId())
                        .stream().filter(Objects::nonNull).map(RoleId::getIdRoleId).toList();
                List<Role> listRole = roleRepository.findAllById(listRoleIds);

                // Convert Role objects to role DTOs
                List<RoleDto> roleDtos = listRole.stream()
                        .map(role -> {
                            RoleDto roleDto = new RoleDto();
                            roleDto.setId(role.getId());
                            roleDto.setName(role.getName());
                            roleDto.setDescription(role.getDescription());
                            return roleDto;
                        })
                        .toList();

                // Get user group
                List<Long> listGroupIds = userGroupMappingRepository.findIdUserGroupIdByIdUserId(user.getId())
                        .stream().filter(Objects::nonNull).map(UserGroupId::getIdUserGroupId).toList();
                List<UserGroup> listUserGroup = userGroupRepository.findAllById(listGroupIds);

                // Convert UserGroup objects to user group DTOs
                List<UserGroupDto> userGroupDtos = listUserGroup.stream()
                        .map(userGroup -> {
                            UserGroupDto userGroupDto = new UserGroupDto();
                            userGroupDto.setId(userGroup.getId());
                            userGroupDto.setName(userGroup.getName());
                            userGroupDto.setDescription(userGroup.getDescription());
                            return userGroupDto;
                        })
                        .toList();

                // Assemble the response
                UserGroupResponse userGroupResponse = new UserGroupResponse();
                userGroupResponse.setId(user.getId());
                userGroupResponse.setFullName(user.getFullName());
                userGroupResponse.setRoles(roleDtos);
                userGroupResponse.setUserGroups(userGroupDtos);

                results.add(userGroupResponse);
            }

            log.info("Get full data for all users finished");
        } catch (Exception e) {
            log.error("Error when getting full data for all users", e);
            rc = ErrorEnum.DEFAULT_ERROR;
        }

        return BaseResponseUtils.constructResponse(rc, results);
    }
}
