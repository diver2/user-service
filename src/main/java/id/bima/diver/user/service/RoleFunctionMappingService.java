package id.bima.diver.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.util.BaseResponseUtils;
import id.bima.diver.user.dto.DeleteRoleFunctionMappingRequestDto;
import id.bima.diver.user.dto.DeleteRoleFunctionMappingResponseDto;
import id.bima.diver.user.dto.FunctionId;
import id.bima.diver.user.dto.GetFunctionIdRequestDto;
import id.bima.diver.user.dto.GetFunctionIdResponseDto;
import id.bima.diver.user.dto.GetRoleIdRequestDto;
import id.bima.diver.user.dto.GetRoleIdResponseDto;
import id.bima.diver.user.dto.RoleFunctionMappingDto;
import id.bima.diver.user.dto.RoleId;
import id.bima.diver.user.entity.RoleFunctionMapping;
import id.bima.diver.user.entity.RoleFunctionMappingPk;
import id.bima.diver.user.repository.RoleFunctionMappingRepository;
import id.bima.diver.user.util.RoleFunctionMappingDtoUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RoleFunctionMappingService extends CompositeDatabaseService<RoleFunctionMappingRepository, RoleFunctionMappingDtoUtil, RoleFunctionMappingDto, RoleFunctionMapping, RoleFunctionMappingPk> {
	
	@Autowired
	protected RoleFunctionMappingService(RoleFunctionMappingRepository repository, RoleFunctionMappingDtoUtil util) {
		super(repository, util);
	}

	public BaseResponse<DeleteRoleFunctionMappingResponseDto> deleteRoleFunctionMappingListByFunctionId(DeleteRoleFunctionMappingRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		DeleteRoleFunctionMappingResponseDto result = null;
		try {
			log.info("Delete role function mapping list by function id started");
			List<RoleFunctionMapping> list = repository.deleteByIdFunctionId(req.getFunctionId());
			result = DeleteRoleFunctionMappingResponseDto.builder()
					.listRoleFunctionMappingDto(util.constructDtoList(list))
					.build();
			log.info("Delete role function mapping list by function id finished");
		} catch (Throwable e) {
			log.error("Error when delete role function mapping list by function id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<DeleteRoleFunctionMappingResponseDto> deleteRoleFunctionMappingListByRoleId(DeleteRoleFunctionMappingRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		DeleteRoleFunctionMappingResponseDto result = null;
		try {
			log.info("Delete role function mapping list by role id started");
			List<RoleFunctionMapping> list = repository.deleteByIdRoleId(req.getRoleId());
			result = DeleteRoleFunctionMappingResponseDto.builder()
					.listRoleFunctionMappingDto(util.constructDtoList(list))
					.build();
			log.info("Delete role function mapping list by role id finished");
		} catch (Throwable e) {
			log.error("Error when delete role function mapping list by role id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetFunctionIdResponseDto> getFunctionIdListByRoleId(GetFunctionIdRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetFunctionIdResponseDto result = null;
		try {
			log.info("Get function id list by role id started");
			List<FunctionId> list = repository.findIdFunctionIdByIdRoleId(req.getRoleId());
			result = GetFunctionIdResponseDto.builder()
					.listFunctionId(list)
					.build();
			log.info("Get function id list by role id finished");
		} catch (Throwable e) {
			log.error("Error when get function id list by role id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetRoleIdResponseDto> getRoleIdListByFunctionId(GetRoleIdRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetRoleIdResponseDto result = null;
		try {
			log.info("Get role id list by function id started");
			List<RoleId> list = repository.findIdRoleIdByIdFunctionId(req.getFunctionId());
			result = GetRoleIdResponseDto.builder()
					.listRoleId(list)
					.build();
			log.info("Get role id list by function id finished");
		} catch (Throwable e) {
			log.error("Error when get role id list by function id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
}
