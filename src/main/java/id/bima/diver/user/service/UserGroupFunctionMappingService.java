package id.bima.diver.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.util.BaseResponseUtils;
import id.bima.diver.user.dto.DeleteUserGroupFunctionMappingRequestDto;
import id.bima.diver.user.dto.DeleteUserGroupFunctionMappingResponseDto;
import id.bima.diver.user.dto.FunctionId;
import id.bima.diver.user.dto.GetFunctionIdRequestDto;
import id.bima.diver.user.dto.GetFunctionIdResponseDto;
import id.bima.diver.user.dto.GetUserGroupIdRequestDto;
import id.bima.diver.user.dto.GetUserGroupIdResponseDto;
import id.bima.diver.user.dto.UserGroupFunctionMappingDto;
import id.bima.diver.user.dto.UserGroupId;
import id.bima.diver.user.entity.UserGroupFunctionMapping;
import id.bima.diver.user.entity.UserGroupFunctionMappingPk;
import id.bima.diver.user.repository.UserGroupFunctionMappingRepository;
import id.bima.diver.user.util.UserGroupFunctionMappingDtoUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserGroupFunctionMappingService extends CompositeDatabaseService<UserGroupFunctionMappingRepository, UserGroupFunctionMappingDtoUtil, UserGroupFunctionMappingDto, UserGroupFunctionMapping, UserGroupFunctionMappingPk> {
	
	@Autowired
	protected UserGroupFunctionMappingService(UserGroupFunctionMappingRepository repository,
			UserGroupFunctionMappingDtoUtil util) {
		super(repository, util);
	}

	public BaseResponse<DeleteUserGroupFunctionMappingResponseDto> deleteUserGroupFunctionMappingListByFunctionId(DeleteUserGroupFunctionMappingRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		DeleteUserGroupFunctionMappingResponseDto result = null;
		try {
			log.info("Delete user group function mapping list by function id started");
			List<UserGroupFunctionMapping> list = repository.deleteByIdFunctionId(req.getFunctionId());
			result = DeleteUserGroupFunctionMappingResponseDto.builder()
					.listUserGroupFunctionMappingDto(util.constructDtoList(list))
					.build();
			log.info("Delete user group function mapping list by function id finished");
		} catch (Throwable e) {
			log.error("Error when delete user group function mapping list by function id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<DeleteUserGroupFunctionMappingResponseDto> deleteUserGroupFunctionMappingListByUserGroupId(DeleteUserGroupFunctionMappingRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		DeleteUserGroupFunctionMappingResponseDto result = null;
		try {
			log.info("Delete user group function mapping list by user group id started");
			List<UserGroupFunctionMapping> list = repository.deleteByIdUserGroupId(req.getUserGroupId());
			result = DeleteUserGroupFunctionMappingResponseDto.builder()
					.listUserGroupFunctionMappingDto(util.constructDtoList(list))
					.build();
			log.info("Delete user group function mapping list by user group id finished");
		} catch (Throwable e) {
			log.error("Error when delete user group function mapping list by user group id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetFunctionIdResponseDto> getFunctionIdListByUserGroupId(GetFunctionIdRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetFunctionIdResponseDto result = null;
		try {
			log.info("Get function id list by user group id started");
			List<FunctionId> list = repository.findIdFunctionIdByIdUserGroupId(req.getUserGroupId());
			result = GetFunctionIdResponseDto.builder()
					.listFunctionId(list)
					.build();
			log.info("Get function id list by user group id finished");
		} catch (Throwable e) {
			log.error("Error when get function id list by user group id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetUserGroupIdResponseDto> getUserGroupIdListByFunctionId(GetUserGroupIdRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetUserGroupIdResponseDto result = null;
		try {
			log.info("Get user group id list by function id started");
			List<UserGroupId> list = repository.findIdUserGroupIdByIdFunctionId(req.getFunctionId());
			result = GetUserGroupIdResponseDto.builder()
					.listUserGroupId(list)
					.build();
			log.info("Get user group id list by function id finished");
		} catch (Throwable e) {
			log.error("Error when get user group id list by function id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
}
