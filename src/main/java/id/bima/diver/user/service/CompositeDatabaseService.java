package id.bima.diver.user.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.dto.CompositeDeleteRequestDto;
import id.bima.diver.common.dto.CompositeDeleteResponseDto;
import id.bima.diver.common.dto.GetResponseDto;
import id.bima.diver.common.dto.SaveRequestDto;
import id.bima.diver.common.dto.SaveResponseDto;
import id.bima.diver.common.util.BaseResponseUtils;
import id.bima.diver.common.util.DtoUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public abstract class CompositeDatabaseService<
		R extends JpaRepository<E, I>, 
		U extends DtoUtil<D, E>, 
		D extends Serializable, 
		E extends Serializable,
		I> {

	protected R repository; 
	protected U util;
	
	@Autowired
	protected CompositeDatabaseService(R repository, U util) {
		this.repository = repository;
		this.util = util;
	}

	public BaseResponse<CompositeDeleteResponseDto<D>> delete(CompositeDeleteRequestDto<D> req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		CompositeDeleteResponseDto<D> result = null;
		try {
			log.info("Delete data started");
			repository.delete(util.constructEntity(req.getDto()));
			result = CompositeDeleteResponseDto.<D>builder()
					.dto(req.getDto())
					.build();
			log.info("Delete data finished");
		} catch (Throwable e) {
			log.error("Error when delete data", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<CompositeDeleteResponseDto<D>> deleteList(CompositeDeleteRequestDto<D> req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		CompositeDeleteResponseDto<D> result = null;
		try {
			log.info("Delete list data started");
			repository.deleteAllInBatch(util.constructEntityList(req.getListDto()));
			result = CompositeDeleteResponseDto.<D>builder()
					.listDto(req.getListDto())
					.build();
			log.info("Delete list data finished");
		} catch (Throwable e) {
			log.error("Error when delete list data", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetResponseDto<D>> getAll() {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetResponseDto<D> result = null;
		try {
			log.info("Get all data started");
			List<E> list = repository.findAll();
			if (!CollectionUtils.isEmpty(list)) {
				result = GetResponseDto.<D>builder()
						.list(util.constructDtoList(list))
						.build();
			}
			log.info("Get all data finished");
		} catch (Throwable e) {
			log.error("Error when get all data", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}

	public BaseResponse<SaveResponseDto<D>> save(SaveRequestDto<D> req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		SaveResponseDto<D> result = null;
		try {
			log.info("Save data started");
			E entity = repository.saveAndFlush(util.constructEntity(req.getDto()));
			result = SaveResponseDto.<D>builder()
					.dto(util.constructDto(entity))
					.build();
			log.info("Save data finished");
		} catch (Throwable e) {
			log.error("Error when save data", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<SaveResponseDto<D>> saveList(SaveRequestDto<D> req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		SaveResponseDto<D> result = null;
		try {
			log.info("Save list data started");
			List<E> list = repository.saveAllAndFlush(util.constructEntityList(req.getList()));
			result = SaveResponseDto.<D>builder()
					.list(util.constructDtoList(list))
					.build();
			log.info("Save list data finished");
		} catch (Throwable e) {
			log.error("Error when save list data", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
}
