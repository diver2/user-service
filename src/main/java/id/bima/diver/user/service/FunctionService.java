package id.bima.diver.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.user.dto.FunctionDto;
import id.bima.diver.user.entity.Function;
import id.bima.diver.user.repository.FunctionRepository;
import id.bima.diver.user.util.FunctionDtoUtil;

@Service
public class FunctionService extends DefaultDatabaseService<FunctionRepository, FunctionDtoUtil, FunctionDto, Function, Long> {

	@Autowired
	protected FunctionService(FunctionRepository repository, FunctionDtoUtil util) {
		super(repository, util);
	}
	
}
