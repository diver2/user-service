package id.bima.diver.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.user.dto.UserGroupDto;
import id.bima.diver.user.entity.UserGroup;
import id.bima.diver.user.repository.UserGroupRepository;
import id.bima.diver.user.util.UserGroupDtoUtil;

@Service
public class UserGroupService extends DefaultDatabaseService<UserGroupRepository, UserGroupDtoUtil, UserGroupDto, UserGroup, Long> {

	@Autowired
	protected UserGroupService(UserGroupRepository repository, UserGroupDtoUtil util) {
		super(repository, util);
	}
	
}
