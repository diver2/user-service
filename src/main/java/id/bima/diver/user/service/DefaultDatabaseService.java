package id.bima.diver.user.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.dto.DeleteRequestDto;
import id.bima.diver.common.dto.DeleteResponseDto;
import id.bima.diver.common.dto.GetRequestDto;
import id.bima.diver.common.dto.GetResponseDto;
import id.bima.diver.common.dto.SaveRequestDto;
import id.bima.diver.common.dto.SaveResponseDto;
import id.bima.diver.common.util.BaseResponseUtils;
import id.bima.diver.common.util.DtoUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public abstract class DefaultDatabaseService<
		R extends JpaRepository<E, I>, 
		U extends DtoUtil<D, E>, 
		D extends Serializable, 
		E extends Serializable,
		I> {

	protected R repository; 
	protected U util;
	
	@Autowired
	protected DefaultDatabaseService(R repository, U util) {
		this.repository = repository;
		this.util = util;
	}

	public BaseResponse<DeleteResponseDto<D, I>> deleteById(DeleteRequestDto<D, I> req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		DeleteResponseDto<D, I> result = null;
		try {
			log.info("Delete data by id started");
			repository.deleteById(req.getId());
			result = DeleteResponseDto.<D, I>builder()
					.id(req.getId())
					.build();
			log.info("Delete data by id finished");
		} catch (Throwable e) {
			log.error("Error when delete data by id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<DeleteResponseDto<D, I>> deleteByIdList(DeleteRequestDto<D, I> req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		DeleteResponseDto<D, I> result = null;
		try {
			log.info("Delete data by id list started");
			repository.deleteAllByIdInBatch(req.getListId());
			result = DeleteResponseDto.<D, I>builder()
					.listId(req.getListId())
					.build();
			log.info("Delete data by id list finished");
		} catch (Throwable e) {
			log.error("Error when delete data by id list", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetResponseDto<D>> getAll() {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetResponseDto<D> result = null;
		try {
			log.info("Get all data started");
			List<E> list = repository.findAll();
			if (!CollectionUtils.isEmpty(list)) {
				result = GetResponseDto.<D>builder()
						.list(util.constructDtoList(list))
						.build();
			}
			log.info("Get all data finished");
		} catch (Throwable e) {
			log.error("Error when get all data", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}

	public BaseResponse<GetResponseDto<D>> getById(GetRequestDto<I> req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetResponseDto<D> result = null;
		try {
			log.info("Get data by id started");
			Optional<E> opt = repository.findById(req.getId());
			if (opt.isPresent()) {
				result = GetResponseDto.<D>builder()
						.dto(util.constructDto(opt.get()))
						.build();
			}
			log.info("Get data by id finished");
		} catch (Throwable e) {
			log.error("Error when get data by id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetResponseDto<D>> getListByListId(GetRequestDto<I> req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetResponseDto<D> result = null;
		try {
			log.info("Get list data by list id started");
			List<E> list = repository.findAllById(req.getList());
			if (!CollectionUtils.isEmpty(list)) {
				result = GetResponseDto.<D>builder()
						.list(util.constructDtoList(list))
						.build();
			}
			log.info("Get list data by list id finished");
		} catch (Throwable e) {
			log.error("Error when get list data by list id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<SaveResponseDto<D>> save(SaveRequestDto<D> req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		SaveResponseDto<D> result = null;
		try {
			log.info("Save data started");
			E entity = repository.saveAndFlush(util.constructEntity(req.getDto()));
			result = SaveResponseDto.<D>builder()
					.dto(util.constructDto(entity))
					.build();
			log.info("Save data finished");
		} catch (Throwable e) {
			log.error("Error when save data", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<SaveResponseDto<D>> saveList(SaveRequestDto<D> req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		SaveResponseDto<D> result = null;
		try {
			log.info("Save list data started");
			List<E> list = repository.saveAllAndFlush(util.constructEntityList(req.getList()));
			result = SaveResponseDto.<D>builder()
					.list(util.constructDtoList(list))
					.build();
			log.info("Save list data finished");
		} catch (Throwable e) {
			log.error("Error when save list data", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
}
