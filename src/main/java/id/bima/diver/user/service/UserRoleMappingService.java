package id.bima.diver.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.util.BaseResponseUtils;
import id.bima.diver.user.dto.DeleteUserRoleMappingRequestDto;
import id.bima.diver.user.dto.DeleteUserRoleMappingResponseDto;
import id.bima.diver.user.dto.GetRoleIdRequestDto;
import id.bima.diver.user.dto.GetRoleIdResponseDto;
import id.bima.diver.user.dto.GetUserIdRequestDto;
import id.bima.diver.user.dto.GetUserIdResponseDto;
import id.bima.diver.user.dto.RoleId;
import id.bima.diver.user.dto.UserId;
import id.bima.diver.user.dto.UserRoleMappingDto;
import id.bima.diver.user.entity.UserRoleMapping;
import id.bima.diver.user.entity.UserRoleMappingPk;
import id.bima.diver.user.repository.UserRoleMappingRepository;
import id.bima.diver.user.util.UserRoleMappingDtoUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserRoleMappingService extends CompositeDatabaseService<UserRoleMappingRepository, UserRoleMappingDtoUtil, UserRoleMappingDto, UserRoleMapping, UserRoleMappingPk> {
	
	@Autowired
	protected UserRoleMappingService(UserRoleMappingRepository repository, UserRoleMappingDtoUtil util) {
		super(repository, util);
	}

	public BaseResponse<DeleteUserRoleMappingResponseDto> deleteUserRoleMappingListByRoleId(DeleteUserRoleMappingRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		DeleteUserRoleMappingResponseDto result = null;
		try {
			log.info("Delete user role mapping list by role id started");
			List<UserRoleMapping> list = repository.deleteByIdRoleId(req.getRoleId());
			result = DeleteUserRoleMappingResponseDto.builder()
					.listUserRoleMappingDto(util.constructDtoList(list))
					.build();
			log.info("Delete user role mapping list by role id finished");
		} catch (Throwable e) {
			log.error("Error when delete user role mapping list by role id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<DeleteUserRoleMappingResponseDto> deleteUserRoleMappingListByUserId(DeleteUserRoleMappingRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		DeleteUserRoleMappingResponseDto result = null;
		try {
			log.info("Delete user role mapping list by user id started");
			List<UserRoleMapping> list = repository.deleteByIdUserId(req.getUserId());
			result = DeleteUserRoleMappingResponseDto.builder()
					.listUserRoleMappingDto(util.constructDtoList(list))
					.build();
			log.info("Delete user role mapping list by user id finished");
		} catch (Throwable e) {
			log.error("Error when delete user role mapping list by user id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetRoleIdResponseDto> getRoleIdListByUserId(GetRoleIdRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetRoleIdResponseDto result = null;
		try {
			log.info("Get role id list by user id started");
			List<RoleId> list = repository.findIdRoleIdByIdUserId(req.getUserId());
			result = GetRoleIdResponseDto.builder()
					.listRoleId(list)
					.build();
			log.info("Get role id list by user id finished");
		} catch (Throwable e) {
			log.error("Error when get role id list by user id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetUserIdResponseDto> getUserIdListByRoleId(GetUserIdRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetUserIdResponseDto result = null;
		try {
			log.info("Get user id list by role id started");
			List<UserId> list = repository.findIdUserIdByIdRoleId(req.getRoleId());
			result = GetUserIdResponseDto.builder()
					.listUserId(list)
					.build();
			log.info("Get user id list by role id finished");
		} catch (Throwable e) {
			log.error("Error when get user id list by role id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
}
