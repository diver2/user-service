package id.bima.diver.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.util.BaseResponseUtils;
import id.bima.diver.user.dto.GetUserAuthRequestDto;
import id.bima.diver.user.dto.GetUserAuthResponseDto;
import id.bima.diver.user.dto.UserAuthDto;
import id.bima.diver.user.entity.UserAuth;
import id.bima.diver.user.repository.UserAuthRepository;
import id.bima.diver.user.util.UserAuthDtoUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserAuthService extends DefaultDatabaseService<UserAuthRepository, UserAuthDtoUtil, UserAuthDto, UserAuth, Long> {
	
	@Autowired
	protected UserAuthService(UserAuthRepository repository, UserAuthDtoUtil util) {
		super(repository, util);
	}

	public BaseResponse<GetUserAuthResponseDto> getListByUserId(GetUserAuthRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetUserAuthResponseDto result = null;
		try {
			log.info("Get user auth list by user id started");
			List<UserAuth> list = repository.findByUserId(req.getUserId());
			if (!CollectionUtils.isEmpty(list)) {
				result = GetUserAuthResponseDto.builder()
						.listUserAuth(util.constructDtoList(list))
						.build();
			}
			
			log.info("Get user auth list by user id finished");
		} catch (Throwable e) {
			log.error("Error when get user auth list by user id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetUserAuthResponseDto> getListByUserIdAndIsDeleted(GetUserAuthRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetUserAuthResponseDto result = null;
		try {
			log.info("Get user auth list by user id and is deleted started");
			List<UserAuth> list = repository.findByUserIdAndIsDeleted(req.getUserId(), req.getIsDeleted());
			if (!CollectionUtils.isEmpty(list)) {
				result = GetUserAuthResponseDto.builder()
						.listUserAuth(util.constructDtoList(list))
						.build();
			}
			
			log.info("Get user auth list by user id and is deleted finished");
		} catch (Throwable e) {
			log.error("Error when get user auth list by user id and is deleted", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetUserAuthResponseDto> getListByUserName(GetUserAuthRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetUserAuthResponseDto result = null;
		try {
			log.info("Get user auth list by user name started");
			List<UserAuth> list = repository.findByUserName(req.getUserName());
			if (!CollectionUtils.isEmpty(list)) {
				result = GetUserAuthResponseDto.builder()
						.listUserAuth(util.constructDtoList(list))
						.build();
			}
			
			log.info("Get user auth list by user name finished");
		} catch (Throwable e) {
			log.error("Error when get user auth list by user name", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetUserAuthResponseDto> getListByUserNameAndIsDeleted(GetUserAuthRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetUserAuthResponseDto result = null;
		try {
			log.info("Get user auth list by user name and is deleted started");
			List<UserAuth> list = repository.findByUserNameAndIsDeleted(req.getUserName(), req.getIsDeleted());
			if (!CollectionUtils.isEmpty(list)) {
				result = GetUserAuthResponseDto.builder()
						.listUserAuth(util.constructDtoList(list))
						.build();
			}
			
			log.info("Get user auth list by user name and is deleted finished");
		} catch (Throwable e) {
			log.error("Error when get user auth list by user name and is deleted ", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
}
