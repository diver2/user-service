package id.bima.diver.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.util.BaseResponseUtils;
import id.bima.diver.user.dto.DeleteUserGroupMappingRequestDto;
import id.bima.diver.user.dto.DeleteUserGroupMappingResponseDto;
import id.bima.diver.user.dto.GetUserGroupIdRequestDto;
import id.bima.diver.user.dto.GetUserGroupIdResponseDto;
import id.bima.diver.user.dto.GetUserIdRequestDto;
import id.bima.diver.user.dto.GetUserIdResponseDto;
import id.bima.diver.user.dto.UserGroupId;
import id.bima.diver.user.dto.UserGroupMappingDto;
import id.bima.diver.user.dto.UserId;
import id.bima.diver.user.entity.UserGroupMapping;
import id.bima.diver.user.entity.UserGroupMappingPk;
import id.bima.diver.user.repository.UserGroupMappingRepository;
import id.bima.diver.user.util.UserGroupMappingDtoUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserGroupMappingService extends CompositeDatabaseService<UserGroupMappingRepository, UserGroupMappingDtoUtil, UserGroupMappingDto, UserGroupMapping, UserGroupMappingPk> {
	
	@Autowired
	protected UserGroupMappingService(UserGroupMappingRepository repository, UserGroupMappingDtoUtil util) {
		super(repository, util);
	}

	public BaseResponse<DeleteUserGroupMappingResponseDto> deleteUserGroupMappingListByUserGroupId(DeleteUserGroupMappingRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		DeleteUserGroupMappingResponseDto result = null;
		try {
			log.info("Delete user group mapping list by user group id started");
			List<UserGroupMapping> list = repository.deleteByIdUserGroupId(req.getUserGroupId());
			result = DeleteUserGroupMappingResponseDto.builder()
					.listUserGroupMappingDto(util.constructDtoList(list))
					.build();
			log.info("Delete user group mapping list by user group id finished");
		} catch (Throwable e) {
			log.error("Error when delete user group mapping list by user group id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<DeleteUserGroupMappingResponseDto> deleteUserGroupMappingListByUserId(DeleteUserGroupMappingRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		DeleteUserGroupMappingResponseDto result = null;
		try {
			log.info("Delete user group mapping list by user id started");
			List<UserGroupMapping> list = repository.deleteByIdUserId(req.getUserId());
			result = DeleteUserGroupMappingResponseDto.builder()
					.listUserGroupMappingDto(util.constructDtoList(list))
					.build();
			log.info("Delete user group mapping list by user id finished");
		} catch (Throwable e) {
			log.error("Error when delete user group mapping list by user id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetUserGroupIdResponseDto> getUserGroupIdListByUserId(GetUserGroupIdRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetUserGroupIdResponseDto result = null;
		try {
			log.info("Get user group id list by user id started");
			List<UserGroupId> list = repository.findIdUserGroupIdByIdUserId(req.getUserId());
			result = GetUserGroupIdResponseDto.builder()
					.listUserGroupId(list)
					.build();
			log.info("Get user group id list by user id finished");
		} catch (Throwable e) {
			log.error("Error when get user group id list by user id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	public BaseResponse<GetUserIdResponseDto> getUserIdListByUserGroupId(GetUserIdRequestDto req) {
		ErrorEnum rc = ErrorEnum.SUCCESS;
		GetUserIdResponseDto result = null;
		try {
			log.info("Get user id list by user group id started");
			List<UserId> list = repository.findIdUserIdByIdUserGroupId(req.getUserGroupId());
			result = GetUserIdResponseDto.builder()
					.listUserId(list)
					.build();
			log.info("Get user id list by user group id finished");
		} catch (Throwable e) {
			log.error("Error when get user id list by user group id", e);
			rc = BaseResponseUtils.getErrorCode(e);
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
}
