package id.bima.diver.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.user.dto.RoleDto;
import id.bima.diver.user.entity.Role;
import id.bima.diver.user.repository.RoleRepository;
import id.bima.diver.user.util.RoleDtoUtil;

@Service
public class RoleService extends DefaultDatabaseService<RoleRepository, RoleDtoUtil, RoleDto, Role, Long> {

	@Autowired
	protected RoleService(RoleRepository repository, RoleDtoUtil util) {
		super(repository, util);
	}
	
}
